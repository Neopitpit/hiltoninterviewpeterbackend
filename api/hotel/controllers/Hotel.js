'use strict';

/**
 * Hotel.js controller
 *
 * @description: A set of functions called "actions" for managing `Hotel`.
 */

module.exports = {

  /**
   * Retrieve hotel records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.hotel.search(ctx.query);
    } else {
      return strapi.services.hotel.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a hotel record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.hotel.fetch(ctx.params);
  },

  /**
   * Count hotel records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.hotel.count(ctx.query);
  },

  /**
   * Create a/an hotel record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.hotel.add(ctx.request.body);
  },

  /**
   * Update a/an hotel record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.hotel.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an hotel record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.hotel.remove(ctx.params);
  }
};

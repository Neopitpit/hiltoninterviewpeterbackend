'use strict';

/**
 * Reservation.js controller
 *
 * @description: A set of functions called "actions" for managing `Reservation`.
 */

module.exports = {

  /**
   * Retrieve reservation records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.reservation.search(ctx.query);
    } else {
      return strapi.services.reservation.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a reservation record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.reservation.fetch(ctx.params);
  },

  /**
   * Count reservation records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.reservation.count(ctx.query);
  },

  /**
   * Create a/an reservation record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.reservation.add(ctx.request.body);
  },

  /**
   * Update a/an reservation record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.reservation.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an reservation record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.reservation.remove(ctx.params);
  }
};
